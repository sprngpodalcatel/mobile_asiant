//
//  AppDelegate.h
//  mobile_asiant
//
//  Created by Pa'One' Nani on 13/12/18.
//  Copyright © 2018 xperiolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

