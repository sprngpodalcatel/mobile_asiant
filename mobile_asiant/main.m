//
//  main.m
//  mobile_asiant
//
//  Created by Pa'One' Nani on 13/12/18.
//  Copyright © 2018 xperiolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
